﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Text;

namespace AppService
{
    class Services
    {
        public static string sElasticServer = "http://103.141.140.153:9404";
        public static string sListService = "finstar|Doctor Đồng;mirae_asset|Mirae Asset;finance_collection_ucash|UCash;finance_collection_paylate|Paylater;finance_collection_wecash|BaGang;finance_collection_vdong|vĐồng";
        //token eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoiMDEyMzQ4ODgwMDgiLCJwaW4iOiIyNjA1ODciLCJpbWVpIjoiZmU5NGIwODYtYWRhOC00Yzg5LTgxMDUtZjUxNTFlMmZhZTViIiwiaWF0IjoxNTcyMzE5OTQ2LCJleHAiOjE1NzIzMjUzNDZ9.sAsN7gg4csPOLZQ-o9D7JNKrJj9FFbLvP6P7KfDAReU
        //finstar
        //121379176
        public static string getMomoRequests(string sService, string sToken, string sBillID, string sPhone)
        {
            HttpWebResponse response;
            string responseText = "";

            if (Request_owa_momo_vn(out response, sService, sToken, sBillID, sPhone))
            {
                responseText = ReadResponse(response);

                response.Close();
            }

            return responseText;
        }

        private static string ReadResponse(HttpWebResponse response)
        {
            using (Stream responseStream = response.GetResponseStream())
            {
                Stream streamToRead = responseStream;

                try
                {
                    if (response.ContentEncoding.ToLower().Contains("gzip"))
                    {
                        streamToRead = new GZipStream(streamToRead, CompressionMode.Decompress);
                    }
                    else if (response.ContentEncoding.ToLower().Contains("deflate"))
                    {
                        streamToRead = new DeflateStream(streamToRead, CompressionMode.Decompress);
                    }
                }
                catch(Exception ex) { }

                using (StreamReader streamReader = new StreamReader(streamToRead, Encoding.UTF8))
                {
                    return streamReader.ReadToEnd();
                }
            }
        }

        private static bool Request_owa_momo_vn(out HttpWebResponse response, string sService, string sToken, string sBillID, string sPhone)
        {
            response = null;

            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://owa.momo.vn/api");

                request.ContentType = "application/json";
                request.Headers.Set(HttpRequestHeader.Authorization, "Bearer " + sToken);
                request.UserAgent = "PostmanRuntime/7.18.0";
                request.Accept = "*/*";
                request.Headers.Set(HttpRequestHeader.CacheControl, "no-cache");
                request.Headers.Add("Postman-Token", @"9b6fd2e8-f989-438b-83fe-e175d7d0e355");
                request.Headers.Set(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                request.KeepAlive = false;
                request.Timeout = 3000;
                request.Method = "POST";
                request.ServicePoint.Expect100Continue = false;

                string body = @"{
                    ""appCode"": ""2.1.27"",
                    ""appVer"": 21271,
                    ""channel"": ""APP"",
                    ""cmdId"": ""1572320500951000000"",
                    ""deviceOS"": ""ANDROID"",
                    ""errorCode"": 0,
                    ""errorDesc"": """",
                    ""lang"": ""vi"",
                    ""momoMsg"": {
                        ""_class"": ""mservice.backend.entity.msg.TranHisMsg"",
                        ""billId"": """ + sBillID + @""",
                        ""category"": 4,
                        ""clientTime"": 1572320500950,
                        ""desc"": ""Ví MoMo"",
                        ""extras"": ""{\""vpc_CardType\"":\""SML\"",\""vpc_TicketNo\"":null}"",
                        ""fee"": 0,
                        ""giftId"": """",
                        ""moneySource"": 1,
                        ""pageNumber"": 1,
                        ""parentTranType"": 3,
                        ""partnerCode"": ""momo"",
                        ""quantity"": 1,
                        ""rowCardId"": null,
                        ""serviceId"": """ + sService + @""",
                        ""serviceName"": ""Doctor Đồng"",
                        ""tranType"": 7,
                        ""useVoucher"": 0,
                        ""user"": """ + sPhone + @"""
                    },
                    ""msgType"": ""NEXT_PAGE_MSG"",
                    ""result"": true,
                    ""time"": 1572320500951,
                    ""user"": """ + sPhone + @"""
                }";
                //body = String.Format(body, sBillID, sService);
                byte[] postBytes = System.Text.Encoding.UTF8.GetBytes(body);
                request.ContentLength = postBytes.Length;
                Stream stream = request.GetRequestStream();
                stream.Write(postBytes, 0, postBytes.Length);
                stream.Close();

                response = (HttpWebResponse)request.GetResponse();
            }
            catch (WebException e)
            {
                if (e.Status == WebExceptionStatus.ProtocolError) response = (HttpWebResponse)e.Response;
                else return false;
            }
            catch (Exception)
            {
                if (response != null) response.Close();
                return false;
            }

            return true;
        }

        /////start
        ///
        public static string MakeRequests()
        {
            HttpWebResponse response;
            string responseText = "";

            if (Request_owa_momo_vn(out response))
            {
                responseText = ReadResponse(response);

                response.Close();
            }
            return responseText;
        }

        private static bool Request_owa_momo_vn(out HttpWebResponse response)
        {
            response = null;

            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://owa.momo.vn/api");

                request.ContentType = "application/json";
                request.Headers.Set(HttpRequestHeader.Authorization, "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoiMDEyMzQ4ODgwMDgiLCJwaW4iOiIyNjA1ODciLCJpbWVpIjoiZmU5NGIwODYtYWRhOC00Yzg5LTgxMDUtZjUxNTFlMmZhZTViIiwiaWF0IjoxNTcyMzMyODA1LCJleHAiOjE1NzIzMzgyMDV9.yvqF72HYwf8NqHf2Sfy0PKHlj1SQBvQfmelUGYSxYbY");
                request.UserAgent = "PostmanRuntime/7.18.0";
                request.Accept = "*/*";
                request.Headers.Set(HttpRequestHeader.CacheControl, "no-cache");
                request.Headers.Add("Postman-Token", @"9b6fd2e8-f989-438b-83fe-e175d7d0e355");
                request.Headers.Set(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                request.KeepAlive = false;

                request.Method = "POST";
                request.ServicePoint.Expect100Continue = false;

                string body = @"{
                    ""appCode"": ""2.1.27"",
                    ""appVer"": 21271,
                    ""channel"": ""APP"",
                    ""cmdId"": ""1572320500951000000"",
                    ""deviceOS"": ""ANDROID"",
                    ""errorCode"": 0,
                    ""errorDesc"": """",
                    ""lang"": ""vi"",
                    ""momoMsg"": {
                        ""_class"": ""mservice.backend.entity.msg.TranHisMsg"",
                        ""billId"": ""121379176"",
                        ""category"": 4,
                        ""clientTime"": 1572320500950,
                        ""desc"": ""Ví MoMo"",
                        ""extras"": ""{\""vpc_CardType\"":\""SML\"",\""vpc_TicketNo\"":null}"",
                        ""fee"": 0,
                        ""giftId"": """",
                        ""moneySource"": 1,
                        ""pageNumber"": 1,
                        ""parentTranType"": 3,
                        ""partnerCode"": ""momo"",
                        ""quantity"": 1,
                        ""rowCardId"": null,
                        ""serviceId"": ""finstar"",
                        ""serviceName"": ""Doctor Đồng"",
                        ""tranType"": 7,
                        ""useVoucher"": 0,
                        ""user"": ""0834888008""
                    },
                    ""msgType"": ""NEXT_PAGE_MSG"",
                    ""result"": true,
                    ""time"": 1572320500951,
                    ""user"": ""0834888008""
                }";
                byte[] postBytes = System.Text.Encoding.UTF8.GetBytes(body);
                request.ContentLength = postBytes.Length;
                Stream stream = request.GetRequestStream();
                stream.Write(postBytes, 0, postBytes.Length);
                stream.Close();

                response = (HttpWebResponse)request.GetResponse();
            }
            catch (WebException e)
            {
                if (e.Status == WebExceptionStatus.ProtocolError) response = (HttpWebResponse)e.Response;
                else return false;
            }
            catch (Exception)
            {
                if (response != null) response.Close();
                return false;
            }

            return true;
        }

        //call login
        public static string getLoginMomo(string sUser, string sPass)
        {
            HttpWebResponse response;
            string responseText = "";

            if (Request_owa_momo_vn_login(out response, sUser, sPass))
            {
                responseText = ReadResponse(response);

                response.Close();
            }

            return responseText;
        }

        private static bool Request_owa_momo_vn_login(out HttpWebResponse response, string sUser, string sPass)
        {
            response = null;

            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://owa.momo.vn/public/login");

                request.ContentType = "application/json";
                request.UserAgent = "PostmanRuntime/7.18.0";
                request.Accept = "*/*";
                request.Headers.Set(HttpRequestHeader.CacheControl, "no-cache");
                request.Headers.Add("Postman-Token", @"f64552d3-e293-4536-b635-c4b31f988278");
                request.Headers.Set(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                request.KeepAlive = false;

                request.Method = "POST";
                request.ServicePoint.Expect100Continue = false;

                string body = @"{
                    ""appCode"": ""2.1.27"",
                    ""appVer"": 21271,
                    ""channel"": ""APP"",
                    ""cmdId"": ""1572342250418000000"",
                    ""deviceOS"": ""ANDROID"",
                    ""errorCode"": 0,
                    ""errorDesc"": """",
                    ""extra"": {
                        ""AAID"": ""1e1f968c-564a-4ef1-943f-a5fdb43951fa"",
                        ""IDFA"": """",
                        ""SIMULATOR"": ""false"",
                        ""pHash"": ""WSBrE/r5JmOo4mt9/IweEzxPxAZDOCA8uCLEORRS7sZ9/0dsv/MoTBhYikSEDRL3""
                    },
                    ""lang"": ""vi"",
                    ""momoMsg"": {
                        ""_class"": ""mservice.backend.entity.msg.LoginMsg"",
                        ""isSetup"": false
                    },
                    ""msgType"": ""USER_LOGIN_MSG"",
                    ""pass"": """ + sPass + @""",
                    ""result"": true,
                    ""time"": " + DateTime.Now.Ticks.ToString() + @",
                    ""user"": """ + sUser + @"""
                }";
                byte[] postBytes = System.Text.Encoding.UTF8.GetBytes(body);
                request.ContentLength = postBytes.Length;
                Stream stream = request.GetRequestStream();
                stream.Write(postBytes, 0, postBytes.Length);
                stream.Close();

                response = (HttpWebResponse)request.GetResponse();
            }
            catch (WebException e)
            {
                if (e.Status == WebExceptionStatus.ProtocolError) response = (HttpWebResponse)e.Response;
                else return false;
            }
            catch (Exception)
            {
                if (response != null) response.Close();
                return false;
            }

            return true;
        }


        //Login Payoo
        public static string LoginPayoo()
        {
            HttpWebResponse response;
            string responseText = "";

            if (Request_app_payoo_vn(out response))
            {
                responseText = ReadResponse(response);

                response.Close();
            }

            return responseText;
        }

        //Get data Payoo
        public static string GetDatapayoo(string auth)
        {
            HttpWebResponse response;
            string responseText = "";

            if (Request_app_payoo_vn(out response))
            {
                responseText = ReadResponse(response);

                response.Close();
            }

            return responseText;
        }

        //VIETUNION: 299523:0:105:R5YYXz6StKHCYxJYzNs4twoTumw=
        //VIETUNION: 299523:0:105:84k5x8k6n51LFV+Hz9zKphmYFr4=
        private static bool Request_app_payoo_vn(out HttpWebResponse response,string sAuth,string s)
        {
            response = null;

            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://app.payoo.vn/api/v1/PayBill/QueryBill");

                request.ContentType = "application/json; charset=UTF-8";
                request.Headers.Set(HttpRequestHeader.AcceptEncoding, "gzip");
                request.UserAgent = "okhttp/3.12.1";
                request.Headers.Set(HttpRequestHeader.Authorization, sAuth);

                request.Method = "POST";
                request.ServicePoint.Expect100Continue = false;

                string body = @"{
                  ""IAppID"": ""299523"",
                  ""CustomerId"": ""034089006569"",
                  ""ProviderId"": ""OCB"",
                  ""ServiceId"": ""TTTG"",
                  ""Language"": 0,
                  ""RequestTimestamp"": 1573030661
                }";
                byte[] postBytes = System.Text.Encoding.UTF8.GetBytes(body);
                request.ContentLength = postBytes.Length;
                Stream stream = request.GetRequestStream();
                stream.Write(postBytes, 0, postBytes.Length);
                stream.Close();

                response = (HttpWebResponse)request.GetResponse();
            }
            catch (WebException e)
            {
                if (e.Status == WebExceptionStatus.ProtocolError) response = (HttpWebResponse)e.Response;
                else return false;
            }
            catch (Exception)
            {
                if (response != null) response.Close();
                return false;
            }

            return true;
        }

        private static bool Request_app_payoo_vn(out HttpWebResponse response)
        {
            response = null;

            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://app.payoo.vn/api/v1/User/Login");

                request.ContentType = "application/json; charset=UTF-8";
                request.Headers.Set(HttpRequestHeader.AcceptEncoding, "gzip");
                request.UserAgent = "okhttp/3.12.1";
                 request.Headers.Set(HttpRequestHeader.Authorization, "VIETUNION: 299523:0:105:bZ/3FtniLdCUyyV/zxPUjds2oYM=");

                request.Method = "POST";
                request.ServicePoint.Expect100Continue = false;

                string body = @"{
                  ""Password"": ""gWUX5PhCQGURWSzIi/x/1w=="",
                  ""UserName"": ""0834888008"",
                  ""Language"": 0,
                  ""RequestTimestamp"": 1573029901
                }";
                byte[] postBytes = System.Text.Encoding.UTF8.GetBytes(body);
                request.ContentLength = postBytes.Length;
                Stream stream = request.GetRequestStream();
                stream.Write(postBytes, 0, postBytes.Length);
                stream.Close();

                response = (HttpWebResponse)request.GetResponse();
            }
            catch (WebException e)
            {
                if (e.Status == WebExceptionStatus.ProtocolError) response = (HttpWebResponse)e.Response;
                else return false;
            }
            catch (Exception)
            {
                if (response != null) response.Close();
                return false;
            }

            return true;
        }


        //PUT elastic
        public static string MakeRequestsPut(string sServer, string index, string type, string id, string data)
        {
            HttpWebResponse response;
            string responseText = "";

            if (putElastic(out response, sServer, index, type, id, data))
            {
                responseText = ReadResponse(response);

                response.Close();
            }

            return responseText;
        }

        //http://35.244.112.211:9200/personal/cmnd/151920351
        private static bool putElastic(out HttpWebResponse response, string sServer, string index, string type, string id, string data)
        {
            response = null;

            try
            {
                string sPath = sServer + "/" + index + "/" + type + "/" + id;
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(sPath);

                request.ContentType = "application/json";
                request.UserAgent = "PostmanRuntime/7.18.0";
                request.Accept = "*/*";
                request.Headers.Set(HttpRequestHeader.CacheControl, "no-cache");
                request.Headers.Add("Postman-Token", @"98d8d3ff-a8af-463e-808f-62671e833dec");
                request.Headers.Set(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                request.KeepAlive = true;

                request.Method = "PUT";

                string body = @"{
                  ""Fullname"": ""vũ đức mạnh"",
                  ""MomoID"": ""1572420232925"",
                  ""BillId"": ""151920351"",
                  ""Phone"": ""+84356286726"",
                  ""Service"": ""Doctor Đồng"",
                  ""ServiceID"": ""finstar"",
                  ""Data"": ""{\""STEP_NAME\"":\""SECOND_PAGE\"",\""header\"":[{\""id\"":\""header\"",\""type\"":\""Header\"",\""label\"":\""Thanh toán dịch vụ\""}],\""footer\"":[{\""id\"":\""footer\"",\""isSaveCheckInfo\"":false,\""type\"":\""ButtonFooter\"",\""isEnable\"":false,\""label\"":\""Thanh toán\"",\""labelBottom\"":\""Bạn sẽ được chuyển sang màn hình tiếp theo để chọn nguồn tiền thanh toán\""}],\""body\"":[{\""type\"":\""View\"",\""id\"":\""body\"",\""regex\"":\""\"",\""children\"":[{\""type\"":\""LogoService\"",\""id\"":\""LogoService\"",\""urlImage\"":\""%serviceIcon%\"",\""serviceName\"":\""%serviceName%\"",\""serviceSite\"":\""%serviceWebSite%\""},{\""type\"":\""RowKeyValue\"",\""id\"":\""RowKeyValue\"",\""groupName\"":\""THÔNG TIN HỢP ĐỒNG\"",\""rows\"":[{\""label\"":\""Số hợp đồng\"",\""value\"":\""151920351\""},{\""label\"":\""Họ tên\"",\""value\"":\""vũ đức mạnh\""},{\""label\"":\""CMND\"",\""value\"":\""299198\""},{\""label\"":\""Tổng nợ\"",\""isAutoFormatMoney\"":true,\""value\"":\""3680000\""}]},{\""type\"":\""Line\""},{\""type\"":\""View\"",\""id_style\"":\""cssPadding\""},{\""type\"":\""TextInputAmount\"",\""id\"":\""originalAmount\"",\""action\"":3,\""placeHolder\"":\""Nhập số cần tiền thanh toán\"",\""keyboardType\"":\""numeric\"",\""value\"":\""\"",\""valueEval\"":\""this.dataResponse.totalAmount\"",\""textLength\"":20,\""scrollTo\"":250,\""minValue\"":20000,\""maxValue\"":20000000,\""errorDescription\"":\""Số tiền phải >=20.000đ và <=20.000.000đ\"",\""textFloating\"":\""Số tiền thanh toán\""},{\""type\"":\""Text\"",\""id_style\"":\""textMinMaxAmount\"",\""minValue\"":20000,\""maxValue\"":\""20000000\"",\""value\"":\""Quý khách có thể thay đổi số tiền thanh toán, giới hạn từ 20.000đ đến 20.000.000đ \""},{\""type\"":\""TextMinMaxAmount_off\"",\""id_style\"":\""textMinMaxAmount\"",\""minValue\"":20000,\""maxValue\"":\""20000000\"",\""value\"":\""Số tiền phải >=20.000đ và <=20.000.000đ\""}]}],\""stylesheet\"":{\""cssPadding\"":{\""height\"":15},\""textMinMaxAmount\"":{\""padding\"":20,\""paddingTop\"":10,\""fontSize\"":14,\""color\"":\""#8f8e94\""}}}"",
                  ""Info"": [
                    {
                      ""label"": ""Số hợp đồng"",
                      ""value"": ""151920351""
                    },
                    {
                      ""label"": ""Họ tên"",
                      ""value"": ""vũ đức mạnh""
                    },
                    {
                      ""label"": ""CMND"",
                      ""value"": ""299198""
                    },
                    {
                      ""label"": ""Tổng nợ"",
                      ""isAutoFormatMoney"": true,
                      ""value"": ""3680000""
                    }
                  ],
                  ""CheckTime"": ""2019-10-30T14:23:53.4228149+07:00""
                }";
                byte[] postBytes = System.Text.Encoding.UTF8.GetBytes(data);
                request.ContentLength = postBytes.Length;
                Stream stream = request.GetRequestStream();
                stream.Write(postBytes, 0, postBytes.Length);
                stream.Close();

                response = (HttpWebResponse)request.GetResponse();
            }
            catch (WebException e)
            {
                if (e.Status == WebExceptionStatus.ProtocolError) response = (HttpWebResponse)e.Response;
                else return false;
            }
            catch (Exception)
            {
                if (response != null) response.Close();
                return false;
            }

            return true;
        }

        //Get data
        public static string getInfoFromID(string id)
        {
            HttpWebResponse response;
            string responseText = "";

            if (Request_info_from_id(out response, id))
            {
                responseText = ReadResponse(response);

                response.Close();
            }

            return responseText;
        }

        //http://35.244.112.211:9200/_all/_search?q=_id:341725773
        private static bool Request_info_from_id(out HttpWebResponse response, string id)
        {
            response = null;
            string sPath = sElasticServer + "/_all/_search?q=_id:" + id;
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(sPath);

                request.UserAgent = "PostmanRuntime/7.19.0";
                request.Accept = "*/*";
                request.Headers.Set(HttpRequestHeader.CacheControl, "no-cache");
                request.Headers.Add("Postman-Token", @"e9afec9d-515b-4f52-8037-ac1c0832987c");
                request.Headers.Set(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                request.KeepAlive = true;

                response = (HttpWebResponse)request.GetResponse();
            }
            catch (WebException e)
            {
                if (e.Status == WebExceptionStatus.ProtocolError) response = (HttpWebResponse)e.Response;
                else return false;
            }
            catch (Exception)
            {
                if (response != null) response.Close();
                return false;
            }

            return true;
        }

    }
}
