﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppService
{
    public class WebApiConfig
    {
        public static MongoClient client = null;// = Utils.getClient();
        public static IMongoDatabase database;// = client.GetDatabase("Vay1h");
        public static IMongoCollection<BsonDocument> colContact;// = database.GetCollection<BsonDocument>("customer");
        public static IMongoCollection<BsonDocument> colUser;
        public static IMongoCollection<BsonDocument> colLicense;
        public static IMongoCollection<BsonDocument> colCustomer;
        public static IMongoCollection<BsonDocument> colPage;
        public static string DBName;

        public static void Register()
        {
            DBName = "leadpro";
            if (client == null)
                client = Utils.getClient();
            database = client.GetDatabase(DBName);
            colContact = database.GetCollection<BsonDocument>("lead");
            colUser = database.GetCollection<BsonDocument>("admin");
            colLicense = database.GetCollection<BsonDocument>("license");
            colCustomer = database.GetCollection<BsonDocument>("customer");
            colPage = database.GetCollection<BsonDocument>("page");
        }


    }
}
