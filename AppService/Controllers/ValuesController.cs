﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;

namespace AppService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        //tim kiem trong elastic
        //http://103.141.140.153:9404/_all/_search?q=_id:063559826

        // GET api/values
        [HttpGet("{id}")]
        public ActionResult<string> Get(string id)
        {

            

            ActionResult<string> sPut = Put(id);

            string sResutlElastic = "";
            sResutlElastic = Services.getInfoFromID(id);
            dynamic jToken = JToken.Parse(sResutlElastic);

            dynamic data = jToken.hits.hits;

            JArray jArr = new JArray();
            foreach (dynamic item in data)
            {
                JObject obj = new JObject();

                obj.Add("FullName", item._source.Fullname);
                obj.Add("Service", item._source.Service);
                obj.Add("Info", item._source.Info);
                
                jArr.Add(obj);
            }


            //Login Payoo
            /*string resLogin = Services.LoginPayoo();
            jToken = JToken.Parse(resLogin);
            if (jToken.Message == "SUCCESS")
            {

            }

            //getdata payoo
            resLogin = Services.GetDatapayoo("VIETUNION: 299523:0:105:84k5x8k6n51LFV+Hz9zKphmYFr4=");
            jToken = JToken.Parse(resLogin);*/


            return jArr.ToString();

        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public ActionResult<string> Put(string id)
        {
            //login

            string sPhone = "0834888008";
            string sPass = "260587";
            string sService = "finstar";
            //string sBillID = "151920351";
            string sBillID = id;
            string resLogin = Services.getLoginMomo(sPhone, sPass);
            dynamic jToken = JToken.Parse(resLogin);
            string sToken = jToken.extra.AUTH_TOKEN;
            string sResultElastic = "";
            string[] sList = Services.sListService.Split(';');

            if (sToken != null)
                for (int i = 0; i < sList.Length; i++)
                {
                    sService = sList[i].Split('|')[0];
                    //i++;

                    //string res = Services.getMomoRequests("finstart", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoiMDEyMzQ4ODgwMDgiLCJwaW4iOiIyNjA1ODciLCJpbWVpIjoiZmU5NGIwODYtYWRhOC00Yzg5LTgxMDUtZjUxNTFlMmZhZTViIiwiaWF0IjoxNTcyMzQxMzY1LCJleHAiOjE1NzIzNDY3NjV9.c04OouDpPx48vwJQR4KhoNA1nORdijgwYA4bUP1b18Q", "121379176");
                    string res = Services.getMomoRequests(sService, sToken, sBillID, sPhone);

                    if (res != "")
                    {
                        jToken = JToken.Parse(res);
                        JObject obj = new JObject();


                        if (jToken.result == true && (sService == "mirae_asset" || sService == "finance_collection_ucash" || sService == "finance_collection_wecash" || sService == "finance_collection_vdong" || sService == "finance_collection_paylate"))
                        {
                            obj.Add("Data", res);
                            //jToken.momoMsg.comment
                            obj.Add("Fullname", jToken.momoMsg.partnerName);
                            obj.Add("MomoID", jToken.momoMsg.tranId);
                            obj.Add("BillId", jToken.momoMsg.billId);
                            obj.Add("Phone", jToken.momoMsg.customerNumber);
                            obj.Add("Service", jToken.momoMsg.serviceName);
                            obj.Add("ServiceID", jToken.momoMsg.serviceId);
                            string sData = jToken.extra.buildFormData;

                            sData = jToken.momoMsg.comment;
                            jToken = JToken.Parse(sData);
                            obj.Add("Info", jToken);
                            obj.Add("CheckTime", DateTime.Now);

                            //Lưu vào Elastic
                            sResultElastic += Services.MakeRequestsPut(Services.sElasticServer, "personal_" + sService, "cmnd", sBillID, obj.ToString());
                        }
                        else if (jToken.result == true)
                        {
                            obj.Add("Data", res);
                            obj.Add("Fullname", jToken.momoMsg.partnerName);
                            obj.Add("MomoID", jToken.momoMsg.partnerRef);
                            obj.Add("BillId", jToken.momoMsg.billId);
                            obj.Add("Phone", jToken.momoMsg.customerNumber);
                            obj.Add("Service", jToken.momoMsg.serviceName);
                            obj.Add("ServiceID", jToken.momoMsg.serviceId);
                            string sData = jToken.extra.buildFormData;

                            jToken = JToken.Parse(sData);
                            obj.Add("Info", jToken.body[0].children[1].rows);
                            obj.Add("CheckTime", DateTime.Now);

                            //Lưu vào Elastic
                            //"result":"created" / "updated"
                            sResultElastic += Services.MakeRequestsPut(Services.sElasticServer, "personal_" + sService, "cmnd", sBillID, obj.ToString());

                        }
                    }

                }

            

            return sResultElastic;
            //return obj.ToString();
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
