﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using MongoDB.Bson.IO;
using MongoDB.Driver;
using Newtonsoft.Json.Linq;

namespace AppService.Controllers
{
    //Nghĩa - 05/03/1987
    [ApiController]
    public class ContactController : ControllerBase
    {

        // GET api/values
        [Route("api/contact/{type}/{value}/")]
        [HttpPost]
        public ActionResult<string> GetPhone(string type, string value)
        {
            var ret = new BsonDocument()
            { };
            ret.Add("code", "001");
            ret.Add("mess", Utils.code001);
            ret.Add("data", new BsonDocument());
            string token = (Request.Headers.Any(x => x.Key == "Authorization")) ? Request.Headers.Where(x => x.Key == "Authorization").FirstOrDefault().Value.SingleOrDefault().Replace("Bearer ", "") : "";
            string key = Utils.MD5(type + "|" + value + "|" + Utils.key);
            //valid data
            if ((type == "phone" || type == "uid") && token == key)
            {
                try
                {

                    var projection = Builders<BsonDocument>.Projection.Include("phone").Include("uid").Include("tag").Exclude("_id");
                    var filter = Builders<BsonDocument>.Filter.Eq(type, value);
                    var document = WebApiConfig.colContact.Find(filter).Project(projection).ToList();

                    if (document != null)
                    {
                        string timeRequest = DateTime.Now.ToString("yyyyMMddHHmmss");
                        //phone = document.GetValue("phone").ToString();
                        ret.Set("code", "200");
                        ret.Set("mess", Utils.code200);
                        ret.Add("time", timeRequest);
                        ret.Set("data", new BsonArray(document));

                        //log thành công
                        //Lưu vào Elastic
                        string sE = Services.MakeRequestsPut(Services.sElasticServer, "logcontact_telepro", type, timeRequest, ret.ToJson());

                    }
                    else
                    {
                        ret.Set("code", "002");
                        ret.Set("mess", Utils.code002);
                        ret.Set("data", new BsonDocument());
                    }
                }
                catch (Exception ex)
                {
                    if (ex.ToString().IndexOf("System.InvalidOperationException: Sequence contains no elements") != -1)
                    {
                        ret.Set("code", "ex");
                        ret.Set("mess", ex.ToString());
                        ret.Set("data", new BsonDocument());
                    }
                    else
                    {
                        ret.Set("code", "002");
                        ret.Set("mess", Utils.code002);
                        ret.Set("data", new BsonDocument());
                    }
                }

            }
            else
            {
                ret.Set("code", "007");
                ret.Set("mess", Utils.code007);
                ret.Set("data", new BsonDocument());
            }

            return ret.ToJson();
        }

        // GET api/values
        [Route("api/contact/info")]
        [HttpPost, HttpGet]
        public ActionResult<string> GetInfoDashboard()
        {
            var ret = new BsonDocument()
            { };
            ret.Add("code", "001");
            ret.Add("mess", Utils.code001);
            ret.Add("data", new BsonDocument());

            try
            {

                var filter = Builders<BsonDocument>.Filter.Empty;
                var count = WebApiConfig.colContact.Find(filter).Count();

                filter = Builders<BsonDocument>.Filter.Exists("details");

                //var countDetails = WebApiConfig.colContact.Find(filter).Count();

                if (count != null)
                {
                    string timeRequest = DateTime.Now.ToString("yyyyMMddHHmmss");
                    //phone = document.GetValue("phone").ToString();
                    ret.Set("code", "200");
                    ret.Set("mess", Utils.code200);
                    ret.Add("time", timeRequest);
                    ret.Set("data", new BsonDocument("total", count.ToString("n0")));
                    ret.Set("data", new BsonDocument("totalFull", "123"));

                    //log thành công
                    //Lưu vào Elastic
                    //string sE = Services.MakeRequestsPut(Services.sElasticServer, "logcontact_telepro", type, timeRequest, ret.ToJson());

                }
                else
                {
                    ret.Set("code", "002");
                    ret.Set("mess", Utils.code002);
                    ret.Set("data", new BsonDocument());
                }
            }
            catch (Exception ex)
            {
                if (ex.ToString().IndexOf("System.InvalidOperationException: Sequence contains no elements") != -1)
                {
                    ret.Set("code", "ex");
                    ret.Set("mess", ex.ToString());
                    ret.Set("data", new BsonDocument());
                }
                else
                {
                    ret.Set("code", "002");
                    ret.Set("mess", Utils.code002);
                    ret.Set("data", new BsonDocument());
                }
            }

            return ret.ToString();
        }

        //lay danh sach friends và nhãn
        [Route("api/contact/friends/{uid}")]
        [HttpPost, HttpGet]
        public ActionResult<string> GetListFriends(string uid)
        {
            var ret = new BsonDocument()
            { };
            ret.Add("code", "001");
            ret.Add("mess", Utils.code001);
            ret.Add("data", new BsonDocument());

            string token = (Request.Headers.Any(x => x.Key == "Authorization")) ? Request.Headers.Where(x => x.Key == "Authorization").FirstOrDefault().Value.SingleOrDefault().Replace("Bearer ", "") : "";
            string key = Utils.MD5(uid + "|" + Utils.key);
            if (token == key)
            {
                try
                {

                    string sResult = getFriends(uid).Result;

                    //var countDetails = WebApiConfig.colContact.Find(filter).Count();

                    if (sResult != "")
                    {
                        string timeRequest = DateTime.Now.ToString("yyyyMMddHHmmss");
                        //phone = document.GetValue("phone").ToString();
                        ret.Set("code", "200");
                        ret.Set("mess", Utils.code200);
                        ret.Add("time", timeRequest);
                        ret.Set("data", sResult);

                        //log thành công
                        //Lưu vào Elastic
                        //string sE = Services.MakeRequestsPut(Services.sElasticServer, "logcontact_telepro", type, timeRequest, ret.ToJson());

                    }
                    else
                    {
                        ret.Set("code", "002");
                        ret.Set("mess", Utils.code002);
                        ret.Set("data", new BsonDocument());
                    }
                }
                catch (Exception ex)
                {
                    if (ex.ToString().IndexOf("System.InvalidOperationException: Sequence contains no elements") != -1)
                    {
                        ret.Set("code", "ex");
                        ret.Set("mess", ex.ToString());
                        ret.Set("data", new BsonDocument());
                    }
                    else
                    {
                        ret.Set("code", "002");
                        ret.Set("mess", Utils.code002);
                        ret.Set("data", new BsonDocument());
                    }
                }
            }

            return ret.ToString();
        }

        //lấy index của một mảng
        //db.customer.aggregate( [ { "$project": { "matchedIndex": { "$indexOfArray": [ "$firends.id", "521385714" ] } } } ] )
        //db.customer.aggregate().match(qb.where("uid").eq("643510898")) .project({ "matchedIndex": { "$indexOfArray": [ "$firends.id", "521385714" ] } })
        //add dữ liệu vào 1 item trong mảng
        //db.getCollection("customer").update({ _id: ObjectId("5e8db30f6174fe1240949a0b") }, { $set: { "firends.8.label": 'L0' } })
        //db.getCollection("customer").update({ uid: "643510898") }, { $set: { "firends.8.label": 'L0' } })

        [Route("api/contact/addlabel/{uid}")]
        [HttpPost, HttpGet]
        public ActionResult<string> AddLabel([FromBody]object label, string uid)
        {
            dynamic dlabel = JObject.Parse(label.ToString());

            //JArray arr = JArray.Parse(friends.ToString());

            var ret = new BsonDocument()
            {
            };
            ret.Add("code", "001");
            ret.Add("mess", Utils.code001);
            ret.Add("data", new BsonDocument());
            string token = (Request.Headers.Any(x => x.Key == "Authorization")) ? Request.Headers.Where(x => x.Key == "Authorization").FirstOrDefault().Value.SingleOrDefault().Replace("Bearer ", "") : "";
            string key = Utils.MD5(uid + "|" + dlabel.uid.ToString() + "|" + dlabel.label.ToString() + "|" + Utils.key);
            //valid data
            if (token == key)
            {

                var options = new UpdateOptions();
                options.IsUpsert = true;

                try
                {
                    var updateDocument = new BsonDocument();
                    //lay ra index
                    int iIndex = getIndexArrayAsync(uid, dlabel.uid.ToString()).Result;

                    if (iIndex > -2)
                    {
                        if(iIndex == -1)
                        {
                            updateDocument = new BsonDocument("$addToSet",
                           new BsonDocument("labels" , BsonDocument.Parse(dlabel.ToString())));
                        }    
                        else
                        {
                            updateDocument = new BsonDocument("$set",
                           new BsonDocument("labels." + (iIndex + 1).ToString(), BsonDocument.Parse(dlabel.ToString())));
                        }    
                        var filter = Builders<BsonDocument>.Filter.Eq("uid", uid);

                        

                        WebApiConfig.colCustomer.UpdateMany(filter, updateDocument, options);
                        if (updateDocument != null)
                        {

                            string timeRequest = DateTime.Now.ToString("yyyyMMddHHmmss");
                            //phone = document.GetValue("phone").ToString();
                            ret.Set("code", "200");
                            ret.Set("mess", Utils.code200);
                            ret.Add("time", timeRequest);
                            //ret.Set("data", new BsonArray(document));
                        }
                        else
                        {
                            ret.Set("code", "002");
                            ret.Set("mess", Utils.code002);
                            ret.Set("data", new BsonDocument());
                        }
                    }
                    else
                    {
                        ret.Set("code", "002");
                        ret.Set("mess", Utils.code002);
                        ret.Set("data", new BsonDocument());
                    }

                }
                catch (Exception ex)
                {
                    if (ex.ToString().IndexOf("System.InvalidOperationException: Sequence contains no elements") != -1)
                    {
                        ret.Set("code", "ex");
                        ret.Set("mess", ex.ToString());
                        ret.Set("data", new BsonDocument());
                    }
                    else
                    {
                        ret.Set("code", "002");
                        ret.Set("mess", Utils.code002);
                        ret.Set("data", new BsonDocument());
                    }
                }
            }

            return ret.ToString();
        }


        public static async Task<string> getFriends(string uid)
        {

            //IMongoClient client = new MongoClient("mongodb://host:port/");
            //IMongoDatabase database = client.GetDatabase("leadpro");
            //IMongoCollection<BsonDocument> collection = database.GetCollection<BsonDocument>("customer");
            string sResult = "";
            // Created with Studio 3T, the IDE for MongoDB - https://studio3t.com/

            BsonDocument filter = new BsonDocument();
            filter.Add("uid", uid);

            BsonDocument projection = new BsonDocument();
            projection.Add("labels", 1.0);
            projection.Add("friendsname", 1.0);
            projection.Add("_id", 0.0);

            var options = new FindOptions<BsonDocument>()
            {
                Projection = projection
            };

            using (var cursor = await WebApiConfig.colCustomer.FindAsync(filter, options))
            {
                while (await cursor.MoveNextAsync())
                {
                    var batch = cursor.Current;
                    foreach (BsonDocument document in batch)
                    {
                        //Console.WriteLine(document.ToJson());
                        sResult = document.ToJson();
                    }
                }
            }

            return sResult;
        }


        public static async Task<int> getIndexArrayAsync(string uid, string findid)
        {
            //IMongoClient client = new MongoClient("mongodb://host:port/");
            //IMongoDatabase database = client.GetDatabase("leadpro");
            //IMongoCollection<BsonDocument> collection = database.GetCollection<BsonDocument>("customer");

            // Created with Studio 3T, the IDE for MongoDB - https://studio3t.com/
            //-2 -> lỗi
            //-1 -> chưa có -> insert thêm vào
            int iResult = -2;
            var options = new AggregateOptions()
            {
                AllowDiskUse = false
            };

            PipelineDefinition<BsonDocument, BsonDocument> pipeline = new BsonDocument[]
            {
                new BsonDocument("$match", new BsonDocument()
                        .Add("uid", uid)),
                new BsonDocument("$project", new BsonDocument()
                        .Add("matchedIndex", new BsonDocument()
                                .Add("$indexOfArray", new BsonArray()
                                        .Add("$labels.uid")
                                        .Add(findid)
                                )
                        ))
            };

            using (var cursor = await WebApiConfig.colCustomer.AggregateAsync(pipeline, options))
            {
                while (await cursor.MoveNextAsync())
                {
                    var batch = cursor.Current;
                    foreach (BsonDocument document in batch)
                    {
                        //Console.WriteLine(document.ToJson());
                        document.Remove("_id");
                        //dynamic dIndex = JObject.Parse(document.ToString());
                        iResult = int.Parse(document.GetElement(0).Value.ToString());
                    }
                }
            }

            return iResult;
        }

        // GET api/values
        [Route("api/contact/add/{uid}")]
        [HttpPost]
        public ActionResult<string> AddFriend([FromBody]object friends, string uid)
        {
            dynamic dfriends = JObject.Parse(friends.ToString());

            //JArray arr = JArray.Parse(friends.ToString());

            var ret = new BsonDocument()
            { };
            ret.Add("code", "001");
            ret.Add("mess", Utils.code001);
            ret.Add("data", new BsonDocument());
            string token = (Request.Headers.Any(x => x.Key == "Authorization")) ? Request.Headers.Where(x => x.Key == "Authorization").FirstOrDefault().Value.SingleOrDefault().Replace("Bearer ", "") : "";
            string key = Utils.MD5(uid + "|" + Utils.key);
            //valid data
            if (token == key)
            {

                var options = new UpdateOptions();
                options.IsUpsert = true;


                try
                {
                    //BsonArray ba = new BsonArray(dfriends.friends);

                    var filter = Builders<BsonDocument>.Filter.Eq("uid", uid);
                    //var updateDocument = new BsonDocument("$set", BsonDocument.Parse(dfriends.ToString()));

                    BsonArray ba = new BsonArray();
                    BsonArray baName = new BsonArray();
                    BsonArray baLabel = new BsonArray();
                    baLabel.Clear();
                    foreach (var item in dfriends.friends)
                    {
                        ba.Add(BsonDocument.Parse("{" + ((Newtonsoft.Json.Linq.JContainer)item).Last.ToString() + "}"));
                        baName.Add(BsonDocument.Parse(item.ToString()));
                        //baLabel.Add(BsonDocument.Parse("{" + ((Newtonsoft.Json.Linq.JContainer)item).Last.ToString() + "}, {\"label\":\"\"}"));
                    }

                    var updateDocument = new BsonDocument("$addToSet",
                       new BsonDocument("friends", new BsonDocument("$each", ba)));

                    WebApiConfig.colCustomer.UpdateMany(filter, updateDocument, options);
                    if (updateDocument != null)
                    {

                        //thêm name
                        updateDocument = new BsonDocument("$set",
                            new BsonDocument("friendsname", baName));

                        WebApiConfig.colCustomer.UpdateMany(filter, updateDocument, options);

                        updateDocument = new BsonDocument("$addToSet",
                            new BsonDocument("labels", baLabel));

                        WebApiConfig.colCustomer.UpdateMany(filter, updateDocument, options);

                        string timeRequest = DateTime.Now.ToString("yyyyMMddHHmmss");
                        //phone = document.GetValue("phone").ToString();
                        ret.Set("code", "200");
                        ret.Set("mess", Utils.code200);
                        ret.Add("time", timeRequest);
                        //ret.Set("data", new BsonArray(document));
                    }
                    else
                    {
                        ret.Set("code", "002");
                        ret.Set("mess", Utils.code002);
                        ret.Set("data", new BsonDocument());
                    }

                }
                catch (Exception ex)
                {
                    if (ex.ToString().IndexOf("System.InvalidOperationException: Sequence contains no elements") != -1)
                    {
                        ret.Set("code", "ex");
                        ret.Set("mess", ex.ToString());
                        ret.Set("data", new BsonDocument());
                    }
                    else
                    {
                        ret.Set("code", "002");
                        ret.Set("mess", Utils.code002);
                        ret.Set("data", new BsonDocument());
                    }
                }

            }
            else
            {
                ret.Set("code", "007");
                ret.Set("mess", Utils.code007);
                ret.Set("data", new BsonDocument());
            }

            return ret.ToJson();
        }

    }



}