﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using MongoDB.Bson.IO;
using MongoDB.Driver;
using Newtonsoft.Json.Linq;

namespace AppService.Controllers
{
    [ApiController]
    public class PageController : ControllerBase
    {

        [Route("api/page/addconv/{pid}")]
        [HttpPost]
        public ActionResult<string> AddConversation([FromBody]object conv, string pid)
        {
            dynamic dconv = JObject.Parse(conv.ToString());

            //JArray arr = JArray.Parse(friends.ToString());

            var ret = new BsonDocument()
            { };
            ret.Add("code", "001");
            ret.Add("mess", Utils.code001);
            ret.Add("data", new BsonDocument());
            string token = (Request.Headers.Any(x => x.Key == "Authorization")) ? Request.Headers.Where(x => x.Key == "Authorization").FirstOrDefault().Value.SingleOrDefault().Replace("Bearer ", "") : "";
            string key = Utils.MD5(pid + "|" + Utils.key);
            //valid data
            if (token == key)
            {

                var options = new UpdateOptions();
                options.IsUpsert = true;


                try
                {
                    //BsonArray ba = new BsonArray(dfriends.friends);

                    var filter = Builders<BsonDocument>.Filter.Eq("pid", pid);
                    //var updateDocument = new BsonDocument("$set", BsonDocument.Parse(dfriends.ToString()));

                    BsonArray ba = new BsonArray();
                    BsonArray baName = new BsonArray();
                    BsonArray baLabel = new BsonArray();
                    baLabel.Clear();
                    foreach (var item in dconv.listconv)
                    {
                        ba.Add(BsonDocument.Parse(item.ToString()));
                        //baName.Add(BsonDocument.Parse(item.ToString()));
                        //baLabel.Add(BsonDocument.Parse("{" + ((Newtonsoft.Json.Linq.JContainer)item).Last.ToString() + "}, {\"label\":\"\"}"));
                    }

                    var updateDocument = new BsonDocument("$addToSet",
                       new BsonDocument("listconv", new BsonDocument("$each", ba)));

                    WebApiConfig.colPage.UpdateMany(filter, updateDocument, options);
                    if (updateDocument != null)
                    {

                        updateDocument = new BsonDocument("$set",
                           new BsonDocument("updatedate" , DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") ));
                        WebApiConfig.colPage.UpdateMany(filter, updateDocument, options);

                        string timeRequest = DateTime.Now.ToString("yyyyMMddHHmmss");
                        //phone = document.GetValue("phone").ToString();
                        ret.Set("code", "200");
                        ret.Set("mess", Utils.code200);
                        ret.Add("time", timeRequest);
                        //ret.Set("data", new BsonArray(document));
                    }
                    else
                    {
                        ret.Set("code", "002");
                        ret.Set("mess", Utils.code002);
                        ret.Set("data", new BsonDocument());
                    }

                }
                catch (Exception ex)
                {
                    if (ex.ToString().IndexOf("System.InvalidOperationException: Sequence contains no elements") != -1)
                    {
                        ret.Set("code", "ex");
                        ret.Set("mess", ex.ToString());
                        ret.Set("data", new BsonDocument());
                    }
                    else
                    {
                        ret.Set("code", "002");
                        ret.Set("mess", Utils.code002);
                        ret.Set("data", new BsonDocument());
                    }
                }

            }
            else
            {
                ret.Set("code", "007");
                ret.Set("mess", Utils.code007);
                ret.Set("data", new BsonDocument());
            }

            return ret.ToJson();
        }

        [Route("api/page/countconv/{pid}")]
        [HttpPost, HttpGet]
        public ActionResult<string> CountConv(string pid)
        {

            var ret = new BsonDocument()
            {
            };
            ret.Add("code", "001");
            ret.Add("mess", Utils.code001);
            ret.Add("data", new BsonDocument());
            string token = (Request.Headers.Any(x => x.Key == "Authorization")) ? Request.Headers.Where(x => x.Key == "Authorization").FirstOrDefault().Value.SingleOrDefault().Replace("Bearer ", "") : "";
            string key = Utils.MD5(pid + "|" + Utils.key);
            //valid data
            if (token == key)
            {
                try
                {
                    var updateDocument = new BsonDocument();
                    //lay ra index
                    int iCount = CountConversation(pid).Result;

                    if (iCount != -1)
                    {
                        string timeRequest = DateTime.Now.ToString("yyyyMMddHHmmss");
                        ret.Set("code", "200");
                        ret.Set("mess", Utils.code200);
                        ret.Set("data", iCount.ToString());
                        ret.Add("time", timeRequest);
                    }
                    else
                    {
                        ret.Set("code", "002");
                        ret.Set("mess", Utils.code002);
                        ret.Set("data", new BsonDocument());
                    }

                }
                catch (Exception ex)
                {
                    if (ex.ToString().IndexOf("System.InvalidOperationException: Sequence contains no elements") != -1)
                    {
                        ret.Set("code", "ex");
                        ret.Set("mess", ex.ToString());
                        ret.Set("data", new BsonDocument());
                    }
                    else
                    {
                        ret.Set("code", "002");
                        ret.Set("mess", Utils.code002);
                        ret.Set("data", new BsonDocument());
                    }
                }
            }

            return ret.ToString();
        }

        public static async Task<int> CountConversation(string pageid)
        {
            int iResult = -1;
            var options = new AggregateOptions()
            {
                AllowDiskUse = false
            };

            PipelineDefinition<BsonDocument, BsonDocument> pipeline = new BsonDocument[]
            {
                new BsonDocument("$match", new BsonDocument()
                        .Add("pid", pageid)),
                new BsonDocument("$project", new BsonDocument()
                        .Add("total", new BsonDocument()
                                .Add("$size", "$listconv")
                        ))
            };

            using (var cursor = await WebApiConfig.colPage.AggregateAsync(pipeline, options))
            {
                while (await cursor.MoveNextAsync())
                {
                    var batch = cursor.Current;
                    foreach (BsonDocument document in batch)
                    {
                        //Console.WriteLine(document.ToJson());
                        document.Remove("_id");
                        iResult = int.Parse(document.GetElement(0).Value.ToString());
                    }
                }
            }

            return iResult;
        }

        [Route("api/page/listconv/{pid}")]
        [HttpPost, HttpGet]
        public ActionResult<string> GetListFriends(string pid)
        {
            var ret = new BsonDocument()
            { };
            ret.Add("code", "001");
            ret.Add("mess", Utils.code001);
            ret.Add("data", new BsonDocument());

            string token = (Request.Headers.Any(x => x.Key == "Authorization")) ? Request.Headers.Where(x => x.Key == "Authorization").FirstOrDefault().Value.SingleOrDefault().Replace("Bearer ", "") : "";
            string key = Utils.MD5(pid + "|" + Utils.key);
            if (token == key)
            {
                try
                {

                    string sResult = GetListConversation(pid).Result;

                    //var countDetails = WebApiConfig.colContact.Find(filter).Count();

                    if (sResult != "")
                    {
                        string timeRequest = DateTime.Now.ToString("yyyyMMddHHmmss");
                        //phone = document.GetValue("phone").ToString();
                        ret.Set("code", "200");
                        ret.Set("mess", Utils.code200);
                        ret.Add("time", timeRequest);
                        ret.Set("data", sResult);

                        //log thành công
                        //Lưu vào Elastic
                        //string sE = Services.MakeRequestsPut(Services.sElasticServer, "logcontact_telepro", type, timeRequest, ret.ToJson());

                    }
                    else
                    {
                        ret.Set("code", "002");
                        ret.Set("mess", Utils.code002);
                        ret.Set("data", new BsonDocument());
                    }
                }
                catch (Exception ex)
                {
                    if (ex.ToString().IndexOf("System.InvalidOperationException: Sequence contains no elements") != -1)
                    {
                        ret.Set("code", "ex");
                        ret.Set("mess", ex.ToString());
                        ret.Set("data", new BsonDocument());
                    }
                    else
                    {
                        ret.Set("code", "002");
                        ret.Set("mess", Utils.code002);
                        ret.Set("data", new BsonDocument());
                    }
                }
            }

            return ret.ToString();
        }

        //get list Conversation from page for sent
        public static async Task<string> GetListConversation(string pageid)
        {
            string sResult = "";
            // Created with Studio 3T, the IDE for MongoDB - https://studio3t.com/

            BsonDocument filter = new BsonDocument();
            filter.Add("pid", pageid);

            BsonDocument projection = new BsonDocument();
            projection.Add("_id", 0.0);

            var options = new FindOptions<BsonDocument>()
            {
                Projection = projection
            };

            using (var cursor = await WebApiConfig.colPage.FindAsync(filter, options))
            {
                while (await cursor.MoveNextAsync())
                {
                    var batch = cursor.Current;
                    foreach (BsonDocument document in batch)
                    {
                        //Console.WriteLine(document.ToJson());
                        sResult = document.ToJson();
                    }
                }
            }

            return sResult;

        }


    }


}