﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using MongoDB.Driver;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace AppService.Controllers
{

    [ApiController]
    public class LeadController : ControllerBase
    {
        [Route("api/lead/addhomelive/{uid}")]
        [HttpPost]
        public ActionResult<string> AddHomeLive([FromBody]object homelive, string uid)
        {
            dynamic dhomelive = JObject.Parse(homelive.ToString());

            //JArray arr = JArray.Parse(friends.ToString());

            var ret = new BsonDocument()
            { };
            ret.Add("code", "001");
            ret.Add("mess", Utils.code001);
            ret.Add("data", new BsonDocument());
            string token = (Request.Headers.Any(x => x.Key == "Authorization")) ? Request.Headers.Where(x => x.Key == "Authorization").FirstOrDefault().Value.SingleOrDefault().Replace("Bearer ", "") : "";
            string key = Utils.MD5(uid + "|" + dhomelive.home.ToString() + "|" + dhomelive.live.ToString() + "|" + Utils.key);
            //valid data
            if (token == key)
            {

                var options = new UpdateOptions();
                options.IsUpsert = true;

                try
                {
                    string home = dhomelive.home.ToString();
                    string live = dhomelive.live.ToString();

                    var filter = Builders<BsonDocument>.Filter.Eq("uid", uid);

                    var update = Builders<BsonDocument>.Update.Set("home", home);

                    UpdateResult ur = WebApiConfig.colContact.UpdateMany(filter, update, options);
                    update = Builders<BsonDocument>.Update.Set("live", live);
                    ur = WebApiConfig.colContact.UpdateMany(filter, update, options);

                    string timeRequest = DateTime.Now.ToString("yyyyMMddHHmmss");
                    //phone = document.GetValue("phone").ToString();
                    ret.Set("code", "200");
                    ret.Set("mess", Utils.code200);
                    ret.Add("time", timeRequest);
                    //ret.Set("data", new BsonArray(document));

                }
                catch (Exception ex)
                {
                    if (ex.ToString().IndexOf("System.InvalidOperationException: Sequence contains no elements") != -1)
                    {
                        ret.Set("code", "ex");
                        ret.Set("mess", ex.ToString());
                        ret.Set("data", new BsonDocument());
                    }
                    else
                    {
                        ret.Set("code", "002");
                        ret.Set("mess", Utils.code002);
                        ret.Set("data", new BsonDocument());
                    }
                }

            }
            else
            {
                ret.Set("code", "007");
                ret.Set("mess", Utils.code007);
                ret.Set("data", new BsonDocument());
            }

            return ret.ToJson();
        }


        [Route("api/lead/listcontact/{pid}")]
        [HttpPost]
        public ActionResult<string> GetListContact([FromBody]object uid, string pid)
        {
            dynamic dUid = JObject.Parse(uid.ToString());
            string sUid = dUid.data;
            var ret = new BsonDocument()
            { };
            ret.Add("code", "001");
            ret.Add("mess", Utils.code001);
            ret.Add("data", new BsonDocument());
            string token = (Request.Headers.Any(x => x.Key == "Authorization")) ? Request.Headers.Where(x => x.Key == "Authorization").FirstOrDefault().Value.SingleOrDefault().Replace("Bearer ", "") : "";
            string key = Utils.MD5(sUid + "|" + pid + "|" + Utils.key);
            //valid data
            if (token == key)
            {


                try
                {
                    BsonArray bResult = new BsonArray();
                    IEnumerable<BsonDocument> batch = GetContact(sUid).Result;
                    foreach (BsonDocument document in batch)
                    {
                        BsonDocument bd = new BsonDocument();
                        bd.Add("uid", document.GetElement("uid").Value.ToString());

                        //document.Remove("_id");
                        //dynamic dDoc = JObject.Parse(document.ToJson());
                        string sHomeTown = "";
                        string sLive = "";
                        string sPhone = "";
                        if (document.Contains("phone") == true)
                        {
                            sPhone = document.GetElement("phone").Value.ToString();
                        }
                        bd.Set("phone", sPhone);
                        if (document.Contains("details") == true)
                        {
                            sHomeTown = document.GetElement("details").Value[0].AsBsonValue.AsBsonDocument.GetElement("basicProfile").Value.AsBsonDocument.GetElement("homeTownCity").Value.ToString();
                            //details.0.basicProfile.currentLocationCity
                            sLive = document.GetElement("details").Value[0].AsBsonValue.AsBsonDocument.GetElement("basicProfile").Value.AsBsonDocument.GetElement("currentLocationCity").Value.ToString();
                        }
                        else
                        {
                            //update basic info
                            //BsonDocument bdRes = UpdateBasicInfo(document.GetElement("uid").Value.ToString());
                            //sHomeTown = bdRes["basicProfile"]["homeTownCity"].ToString();
                            //sLive = bdRes["basicProfile"]["currentLocationCity"].ToString();

                            Thread myNewThreadMess = new Thread(() => UpdateBasicInfo(document.GetElement("uid").Value.ToString()));
                            myNewThreadMess.Start();

                            sHomeTown = "Đang cập nhật - tải lại sau 5 phút";
                            sLive = "Đang cập nhật - tải lại sau 5 phút";
                        }

                        if (document.Contains("home") == true)
                        {
                            if (document.GetElement("home").Value.ToString() != "")
                                sHomeTown = document.GetElement("home").Value.ToString();
                        }
                        if (document.Contains("live") == true)
                        {
                            if (document.GetElement("live").Value.ToString() != "")
                                sLive = document.GetElement("live").Value.ToString();
                        }

                        bd.Set("home", sHomeTown);
                        bd.Set("live", sLive);
                        bResult.Add(bd);
                        //Console.WriteLine(document.ToJson());
                    }



                    string timeRequest = DateTime.Now.ToString("yyyyMMddHHmmss");
                    //phone = document.GetValue("phone").ToString();
                    ret.Set("code", "200");
                    ret.Set("mess", Utils.code200);
                    ret.Add("time", timeRequest);
                    ret.Set("data", bResult.ToJson());

                }
                catch (Exception ex)
                {
                    if (ex.ToString().IndexOf("System.InvalidOperationException: Sequence contains no elements") != -1)
                    {
                        ret.Set("code", "ex");
                        ret.Set("mess", ex.ToString());
                        ret.Set("data", new BsonDocument());
                    }
                    else
                    {
                        ret.Set("code", "002");
                        ret.Set("mess", Utils.code002);
                        ret.Set("data", new BsonDocument());
                    }
                }

            }
            else
            {
                ret.Set("code", "007");
                ret.Set("mess", Utils.code007);
                ret.Set("data", new BsonDocument());
            }

            return ret.ToJson();
        }

        public static async Task<IEnumerable<BsonDocument>> GetContact(string uid)
        {
            IEnumerable<BsonDocument> batch = null;
            //IMongoClient client = new MongoClient("mongodb://host:port/");
            //IMongoDatabase database = client.GetDatabase("leadpro");
            //IMongoCollection<BsonDocument> collection = database.GetCollection<BsonDocument>("lead");

            // Created with Studio 3T, the IDE for MongoDB - https://studio3t.com/
            BsonDocument sResult = new BsonDocument();
            string[] aUid = uid.Split(',');
            BsonDocument filter = new BsonDocument();

            BsonArray bUid = new BsonArray();
            foreach (var item in aUid)
            {
                bUid.Add(item.ToString());
            }

            filter.Add("uid", new BsonDocument()
                    .Add("$in", bUid
                    )
            );

            batch = WebApiConfig.colContact.Find(filter).ToEnumerable();

            //using (var cursor = await WebApiConfig.colContact.FindAsync(filter))
            //{
            //    while (await cursor.MoveNextAsync())
            //    {
            //        batch = cursor.Current;
            //sResult = cursor.Current.ToBsonDocument();
            /*foreach (BsonDocument document in batch)
            {
                Console.WriteLine(document.ToJson());
            }*/
            //batch.Intersect(cursor.Current);
            //   }

            //}

            return batch;

        }

        [Route("api/lead/updateinfo")]
        [HttpPost]
        public ActionResult<string> UpdateInfoLead([FromBody]object uid)
        {
            dynamic duid = JObject.Parse(uid.ToString());

            //JArray arr = JArray.Parse(friends.ToString());

            var ret = new BsonDocument()
            { };
            ret.Add("code", "001");
            ret.Add("mess", Utils.code001);
            ret.Add("data", new BsonDocument());
            string token = (Request.Headers.Any(x => x.Key == "Authorization")) ? Request.Headers.Where(x => x.Key == "Authorization").FirstOrDefault().Value.SingleOrDefault().Replace("Bearer ", "") : "";
            string key = Utils.MD5(duid.uid.ToString() + "|" + duid.phone.ToString() + "|" + Utils.key);
            //valid data
            if (token == key)
            {

                var options = new UpdateOptions();
                options.IsUpsert = true;

                try
                {
                    string id = duid.uid.ToString();
                    string phone = duid.phone.ToString();

                    UpdateBasicInfo(phone, id);

                    string timeRequest = DateTime.Now.ToString("yyyyMMddHHmmss");
                    //phone = document.GetValue("phone").ToString();
                    ret.Set("code", "200");
                    ret.Set("mess", Utils.code200);
                    ret.Add("time", timeRequest);
                    //ret.Set("data", new BsonArray(document));

                }
                catch (Exception ex)
                {
                    if (ex.ToString().IndexOf("System.InvalidOperationException: Sequence contains no elements") != -1)
                    {
                        ret.Set("code", "ex");
                        ret.Set("mess", ex.ToString());
                        ret.Set("data", new BsonDocument());
                    }
                    else
                    {
                        ret.Set("code", "002");
                        ret.Set("mess", Utils.code002);
                        ret.Set("data", new BsonDocument());
                    }
                }

            }
            else
            {
                ret.Set("code", "007");
                ret.Set("mess", Utils.code007);
                ret.Set("data", new BsonDocument());
            }

            return ret.ToJson();
        }

        [Route("api/lead/updateinfofromid")]
        [HttpPost]
        public ActionResult<string> UpdateInfoLeadFromUID([FromBody]object uid)
        {
            dynamic duid = JObject.Parse(uid.ToString());

            //JArray arr = JArray.Parse(friends.ToString());

            var ret = new BsonDocument()
            { };
            ret.Add("code", "001");
            ret.Add("mess", Utils.code001);
            ret.Add("data", new BsonDocument());
            string token = (Request.Headers.Any(x => x.Key == "Authorization")) ? Request.Headers.Where(x => x.Key == "Authorization").FirstOrDefault().Value.SingleOrDefault().Replace("Bearer ", "") : "";
            string key = Utils.MD5(duid.uid.ToString() + "|" + duid.phone.ToString() + "|" + Utils.key);
            //valid data
            if (token == key)
            {

                var options = new UpdateOptions();
                options.IsUpsert = true;

                try
                {
                    string id = duid.uid.ToString();
                    string phone = duid.phone.ToString();

                    BsonDocument bd = UpdateBasicInfo(id);

                    string timeRequest = DateTime.Now.ToString("yyyyMMddHHmmss");
                    //phone = document.GetValue("phone").ToString();
                    ret.Set("code", "200");
                    ret.Set("mess", Utils.code200);
                    ret.Add("time", timeRequest);
                    ret.Set("data", bd.GetElement("basicProfile").Value.ToString());

                }
                catch (Exception ex)
                {
                    if (ex.ToString().IndexOf("System.InvalidOperationException: Sequence contains no elements") != -1)
                    {
                        ret.Set("code", "ex");
                        ret.Set("mess", ex.ToString());
                        ret.Set("data", new BsonDocument());
                    }
                    else
                    {
                        ret.Set("code", "002");
                        ret.Set("mess", Utils.code002);
                        ret.Set("data", new BsonDocument());
                    }
                }

            }
            else
            {
                ret.Set("code", "007");
                ret.Set("mess", Utils.code007);
                ret.Set("data", new BsonDocument());
            }

            return ret.ToJson();
        }



        public static bool UpdateBasicInfo(string phone, string uid)
        {
            bool bRes = false;
            var client = new RestClient("http://18.138.46.64:8082/facebook-information/?oauth_signature_method=HMAC-SHA1&oauth_timestamp=1577097152&oauth_nonce=dBbnhP&oauth_version=1.0&oauth_signature=JhVGQDR0HG0yPDALG6lwD26uOco=&id=" + phone + "&typeId=PHONE&fields=basic_profile,posts,works,family,friends,education,groups");
            var request = new RestRequest(Method.GET);
            request.AddHeader("postman-token", "79bcf4db-11c9-ca99-4954-8db0ad4486cd");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("authorization", "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ0aW1hLWl0MiIsImF1dGhvcml0aWVzIjpbIlJPTEVfRkFDRUJPT0tfSU5GTyJdLCJpYXQiOjE1ODczMTAyMTgsImV4cCI6MTU4ODYwNjIxOH0.47EnlTvDpshQ-WWbP4fr_Ki2rryfP7dYOTohnq4NVM0zeSOTUaxhnXP32uPsivzJ6A3MHJZx12v-NYW6DNThhA");
            IRestResponse response = client.Execute(request);
            dynamic r = JToken.Parse(response.Content);

            if (((Newtonsoft.Json.Linq.JValue)r.result.status).Value.ToString() == "DONE")
            {

                string sbasicProfile = ((Newtonsoft.Json.Linq.JProperty)((Newtonsoft.Json.Linq.JToken)r.result.detail.basicProfile).Parent).Value.ToString();
                BsonDocument basicProfile = BsonDocument.Parse(sbasicProfile);
                string works = ((Newtonsoft.Json.Linq.JProperty)((Newtonsoft.Json.Linq.JToken)r.result.detail.works).Parent).Value.ToString();
                JArray a = JArray.Parse(works);
                BsonArray barrWork = new BsonArray();
                BsonDocument bdoc = new BsonDocument();

                BsonDocument details = BsonDocument.Parse(((Newtonsoft.Json.Linq.JProperty)((Newtonsoft.Json.Linq.JToken)r.result.detail).Parent).Value.ToString());


                foreach (var i in a.Children())
                {
                    barrWork.Add(BsonDocument.Parse(i.ToString()));
                }

                var options = new UpdateOptions();
                options.IsUpsert = true;

                BsonArray arrBI = new BsonArray();
                arrBI.Add(basicProfile);

                try
                {

                    var filter = Builders<BsonDocument>.Filter.Eq("phone", phone);
                    filter = filter & Builders<BsonDocument>.Filter.Eq("uid", uid);

                    var updateDocument = new BsonDocument("$addToSet", new BsonDocument
                            {
                                { "details", details }
                            });

                    WebApiConfig.colContact.UpdateMany(filter, updateDocument, options);
                    bRes = true;
                    //Console.WriteLine("Update done : " + uid + " - phone - " + phone);
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            else
            {
                //Console.WriteLine("Khong co data");
                bRes = false;
            }

            return bRes;
        }

        [Route("api/lead/updatebasicinfo")]
        [HttpPost]
        public BsonDocument UpdateBasicInfo(string uid)
        {
            BsonDocument bRes = new BsonDocument();
            var client = new RestClient("http://18.138.46.64:8082/facebook-information/?oauth_signature_method=HMAC-SHA1&oauth_timestamp=1577097152&oauth_nonce=dBbnhP&oauth_version=1.0&oauth_signature=JhVGQDR0HG0yPDALG6lwD26uOco=&id=" + uid + "&typeId=UID&fields=basic_profile,posts,works,family,friends,education,groups");
            var request = new RestRequest(Method.GET);
            request.AddHeader("postman-token", "79bcf4db-11c9-ca99-4954-8db0ad4486cd");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("authorization", "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ0aW1hLWl0MiIsImF1dGhvcml0aWVzIjpbIlJPTEVfRkFDRUJPT0tfSU5GTyJdLCJpYXQiOjE1ODczMTAyMTgsImV4cCI6MTU4ODYwNjIxOH0.47EnlTvDpshQ-WWbP4fr_Ki2rryfP7dYOTohnq4NVM0zeSOTUaxhnXP32uPsivzJ6A3MHJZx12v-NYW6DNThhA");
            IRestResponse response = client.Execute(request);
            dynamic r = JToken.Parse(response.Content);

            if (((Newtonsoft.Json.Linq.JValue)r.result.status).Value.ToString() == "DONE")
            {

                string sbasicProfile = ((Newtonsoft.Json.Linq.JProperty)((Newtonsoft.Json.Linq.JToken)r.result.detail.basicProfile).Parent).Value.ToString();
                BsonDocument basicProfile = BsonDocument.Parse(sbasicProfile);
                string works = ((Newtonsoft.Json.Linq.JProperty)((Newtonsoft.Json.Linq.JToken)r.result.detail.works).Parent).Value.ToString();
                JArray a = JArray.Parse(works);
                BsonArray barrWork = new BsonArray();
                BsonDocument bdoc = new BsonDocument();

                BsonDocument details = BsonDocument.Parse(((Newtonsoft.Json.Linq.JProperty)((Newtonsoft.Json.Linq.JToken)r.result.detail).Parent).Value.ToString());


                foreach (var i in a.Children())
                {
                    barrWork.Add(BsonDocument.Parse(i.ToString()));
                }

                var options = new UpdateOptions();
                options.IsUpsert = true;

                BsonArray arrBI = new BsonArray();
                arrBI.Add(basicProfile);

                try
                {

                    var filter = Builders<BsonDocument>.Filter.Eq("uid", uid);

                    var updateDocument = new BsonDocument("$addToSet", new BsonDocument
                            {
                                { "details", details }
                            });

                    WebApiConfig.colContact.UpdateMany(filter, updateDocument, options);
                    bRes = details;
                    //Console.WriteLine("Update done : " + uid + " - phone - " + phone);
                }
                catch (Exception ex)
                {
                    return bRes = new BsonDocument();
                }
            }
            else
            {
                //Console.WriteLine("Khong co data");
                bRes = new BsonDocument();
            }

            return bRes;
        }
    }
}