﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using MongoDB.Bson.IO;
using MongoDB.Driver;

namespace AppService.Controllers
{
    [ApiController]
    public class UserController : ControllerBase
    {

        // GET api/login
        [Route("api/user/login/")]
        [HttpPost]
        public ActionResult<string> Login(string user, string password)
        {
            user = Request.Form["username"].ToString();
            password = Request.Form["password"].ToString();
            var ret = new BsonDocument()
            { };
            ret.Add("code", "001");

            ret.Add("mess", Utils.code001);
            ret.Add("data", new BsonDocument());

            BsonArray bsData = new BsonArray();
            bsData.Add(new BsonDocument().Set("username", "invalid"));

            string token = (Request.Headers.Any(x => x.Key == "Authorization")) ? Request.Headers.Where(x => x.Key == "Authorization").FirstOrDefault().Value.SingleOrDefault().Replace("Bearer ", "") : "";
            string key = Utils.MD5(user + "|" + password + "|" + Utils.key);
            //valid data
            if (token != key)
            {
                try
                {

                    var projection = Builders<BsonDocument>.Projection.Exclude("_id").Exclude("password").Exclude("createddate");
                    var filter = Builders<BsonDocument>.Filter.Eq("username", user);
                    filter = filter & Builders<BsonDocument>.Filter.Eq("password", password);
                    var document = WebApiConfig.colUser.Find(filter).Project(projection).ToList();


                    if (document != null)
                    {
                        string timeRequest = DateTime.Now.ToString("yyyyMMddHHmmss");
                        //phone = document.GetValue("phone").ToString();
                        ret.Set("code", "200");
                        ret.Set("mess", Utils.code200);
                        ret.Add("time", timeRequest);

                        if (document.Count > 0)
                            ret.Set("data", new BsonArray(document));
                        else
                            ret.Set("data", bsData);

                        //log thành công
                        //Lưu vào Elastic
                        //string sE = Services.MakeRequestsPut(Services.sElasticServer, "logcontact_telepro", type, timeRequest, ret.ToJson());

                    }
                    else
                    {
                        ret.Set("code", "002");
                        ret.Set("mess", Utils.code002);
                        ret.Set("data", bsData);
                    }
                }
                catch (Exception ex)
                {
                    if (ex.ToString().IndexOf("System.InvalidOperationException: Sequence contains no elements") != -1)
                    {
                        ret.Set("code", "ex");
                        ret.Set("mess", ex.ToString());
                        ret.Set("data", bsData);
                    }
                    else
                    {
                        ret.Set("code", "002");
                        ret.Set("mess", Utils.code002);
                        ret.Set("data", bsData);
                    }
                }

            }
            else
            {
                ret.Set("code", "007");
                ret.Set("mess", Utils.code007);
                ret.Set("data", bsData);
            }

            return ret.ToJson();
        }


        [Route("api/user/checkactive/{uid}/{activekey}/{accesstoken}")]
        [HttpPost]
        public ActionResult<string> CheckActive(string uid, string activekey,string accesstoken)
        {
            var ret = new BsonDocument()
            { };
            ret.Add("code", "001");

            ret.Add("mess", Utils.code001);
            ret.Add("data", new BsonDocument());

            BsonArray bsData = new BsonArray();
            bsData.Add(new BsonDocument().Set("uid", "invalid"));

            string token = (Request.Headers.Any(x => x.Key == "Authorization")) ? Request.Headers.Where(x => x.Key == "Authorization").FirstOrDefault().Value.SingleOrDefault().Replace("Bearer ", "") : "";
            string key = Utils.MD5(uid + "|" + activekey + "|" + Utils.key);
            //valid data
            if (token == key)
            {
                try
                {

                    var projection = Builders<BsonDocument>.Projection.Exclude("_id").Exclude("expdate");
                    var filter = Builders<BsonDocument>.Filter.Eq("uid", uid);
                    filter = filter & Builders<BsonDocument>.Filter.Eq("activekey", activekey);
                    filter = filter & Builders<BsonDocument>.Filter.Gte("expdate", DateTime.Now);
                    var document = WebApiConfig.colLicense.Find(filter).Project(projection).ToList();


                    if (document != null)
                    {
                        string timeRequest = DateTime.Now.ToString("yyyyMMddHHmmss");
                        //phone = document.GetValue("phone").ToString();
                        ret.Set("code", "200");
                        ret.Set("mess", Utils.code200);
                        ret.Add("time", timeRequest);

                        if (document.Count > 0)
                        {
                            document[0].Set("accesstoken", accesstoken);

                            var update = Builders<BsonDocument>.Update.Set("accesstoken", accesstoken);

                            WebApiConfig.colLicense.UpdateOne(filter, update);
                            ret.Set("data", new BsonArray(document));
                        }
                        else
                        {
                            //ret.Set("data", bsData);

                            //Kiểm tra xem có uid này chưa, chưa có thì sẽ set cho 7 ngày dùng thử, có rồi thì bỏ qua
                            filter = Builders<BsonDocument>.Filter.Eq("uid", uid);
                            document = WebApiConfig.colLicense.Find(filter).Project(projection).ToList();
                            if (document != null)
                            {
                                if (document.Count == 0)
                                {
                                    //Thêm uid và cho dùng thử 7 ngày
                                    var newdoc = new BsonDocument
                                    {
                                    };
                                    newdoc.Add("uid", uid);
                                    newdoc.Add("activekey", "IDOTRIAL");
                                    newdoc.Add("expdate", DateTime.Now.AddDays(7));
                                    newdoc.Add("accesstoken", accesstoken);
                                    WebApiConfig.colLicense.InsertOne(newdoc);
                                    //newdoc.Remove("expdate");
                                    newdoc.Remove("_id");
                                    bsData = new BsonArray();
                                    bsData.Add(newdoc);
                                    ret.Set("data", bsData);
                                }
                                else
                                {
                                    //hết hạn
                                    ret.Set("code", "002");
                                    ret.Set("mess", Utils.code003);
                                    ret.Set("data", bsData);
                                }
                            }
                            else
                            {
                                ret.Set("data", bsData);
                            }

                        }

                        //log thành công
                        //Lưu vào Elastic
                        //string sE = Services.MakeRequestsPut(Services.sElasticServer, "logcontact_telepro", type, timeRequest, ret.ToJson());

                    }
                    else
                    {
                        ret.Set("code", "002");
                        ret.Set("mess", Utils.code002);
                        ret.Set("data", bsData);
                    }
                }
                catch (Exception ex)
                {
                    if (ex.ToString().IndexOf("System.InvalidOperationException: Sequence contains no elements") != -1)
                    {
                        ret.Set("code", "ex");
                        ret.Set("mess", ex.ToString());
                        ret.Set("data", bsData);
                    }
                    else
                    {
                        ret.Set("code", "002");
                        ret.Set("mess", Utils.code002);
                        ret.Set("data", bsData);
                    }
                }

            }
            else
            {
                ret.Set("code", "007");
                ret.Set("mess", Utils.code007);
                ret.Set("data", bsData);
            }

            return ret.ToJson();
        }



    }



}