﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace AppService
{
    public class Utils
    {
        public const string key = "loirubuon6868";
        public const string code200 = "success";
        public const string code001 = "no data";
        public const string code002 = "request success no data";
        public const string code007 = "no valid data";
        public const string code003 = "expired";
        public static MongoClient getClient()
        {
            //string sConnection = "mongodb+srv://wind:nghiatrang@cluster0-info-swklk.mongodb.net/shoppro?retryWrites=true";
            //string sConnection = "mongodb://adminleadpro:adminleadpro%40123@34.87.30.71:27017/leadpro?authSource=leadpro";
            string sConnection = "mongodb://adminleadpro:adminleadpro%40123@157.245.53.37:27017,167.71.218.35:27017,157.245.58.108:27017/?replicaSet=shopf1-mongodb-set&authSource=leadpro";
            var client = new MongoClient(sConnection);
            return client;
        }

        public static string MD5(string s)
        {
            StringBuilder hash = new StringBuilder();
            MD5CryptoServiceProvider md5provider = new MD5CryptoServiceProvider();
            byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(s));

            for (int i = 0; i < bytes.Length; i++)
            {
                hash.Append(bytes[i].ToString("x2"));
            }
            return hash.ToString();
        }

    }
}
