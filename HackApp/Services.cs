﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Text;

namespace HackApp
{
    class Services
    {
        //token eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoiMDEyMzQ4ODgwMDgiLCJwaW4iOiIyNjA1ODciLCJpbWVpIjoiZmU5NGIwODYtYWRhOC00Yzg5LTgxMDUtZjUxNTFlMmZhZTViIiwiaWF0IjoxNTcyMzE5OTQ2LCJleHAiOjE1NzIzMjUzNDZ9.sAsN7gg4csPOLZQ-o9D7JNKrJj9FFbLvP6P7KfDAReU
        //finstar
        //121379176
        public static string getMomoRequests(string sService, string sToken, string sBillID)
        {
            HttpWebResponse response;
            string responseText = "";

            if (Request_owa_momo_vn(out response, sService, sToken, sBillID))
            {
                responseText = ReadResponse(response);

                response.Close();
            }

            return responseText;
        }

        private static string ReadResponse(HttpWebResponse response)
        {
            using (Stream responseStream = response.GetResponseStream())
            {
                Stream streamToRead = responseStream;
                if (response.ContentEncoding.ToLower().Contains("gzip"))
                {
                    streamToRead = new GZipStream(streamToRead, CompressionMode.Decompress);
                }
                else if (response.ContentEncoding.ToLower().Contains("deflate"))
                {
                    streamToRead = new DeflateStream(streamToRead, CompressionMode.Decompress);
                }

                using (StreamReader streamReader = new StreamReader(streamToRead, Encoding.UTF8))
                {
                    return streamReader.ReadToEnd();
                }
            }
        }

        private static bool Request_owa_momo_vn(out HttpWebResponse response, string sService, string sToken, string sBillID)
        {
            response = null;

            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://owa.momo.vn/api");

                request.ContentType = "application/json";
                request.Headers.Set(HttpRequestHeader.Authorization, "Bearer " + sToken);
                request.UserAgent = "PostmanRuntime/7.18.0";
                request.Accept = "*/*";
                request.Headers.Set(HttpRequestHeader.CacheControl, "no-cache");
                request.Headers.Add("Postman-Token", @"9b6fd2e8-f989-438b-83fe-e175d7d0e355");
                request.Headers.Set(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                request.KeepAlive = false;

                request.Method = "POST";
                request.ServicePoint.Expect100Continue = false;

                string body = @"{
                    ""appCode"": ""2.1.27"",
                    ""appVer"": 21271,
                    ""channel"": ""APP"",
                    ""cmdId"": ""1572320500951000000"",
                    ""deviceOS"": ""ANDROID"",
                    ""errorCode"": 0,
                    ""errorDesc"": """",
                    ""lang"": ""vi"",
                    ""momoMsg"": {
                        ""_class"": ""mservice.backend.entity.msg.TranHisMsg"",
                        ""billId"": """ + sBillID + @""",
                        ""category"": 4,
                        ""clientTime"": 1572320500950,
                        ""desc"": ""Ví MoMo"",
                        ""extras"": ""{\""vpc_CardType\"":\""SML\"",\""vpc_TicketNo\"":null}"",
                        ""fee"": 0,
                        ""giftId"": """",
                        ""moneySource"": 1,
                        ""pageNumber"": 1,
                        ""parentTranType"": 3,
                        ""partnerCode"": ""momo"",
                        ""quantity"": 1,
                        ""rowCardId"": null,
                        ""serviceId"": """ + sService + @""",
                        ""serviceName"": ""Doctor Đồng"",
                        ""tranType"": 7,
                        ""useVoucher"": 0,
                        ""user"": ""0834888008""
                    },
                    ""msgType"": ""NEXT_PAGE_MSG"",
                    ""result"": true,
                    ""time"": 1572320500951,
                    ""user"": ""0834888008""
                }";
                //body = String.Format(body, sBillID, sService);
                byte[] postBytes = System.Text.Encoding.UTF8.GetBytes(body);
                request.ContentLength = postBytes.Length;
                Stream stream = request.GetRequestStream();
                stream.Write(postBytes, 0, postBytes.Length);
                stream.Close();

                response = (HttpWebResponse)request.GetResponse();
            }
            catch (WebException e)
            {
                if (e.Status == WebExceptionStatus.ProtocolError) response = (HttpWebResponse)e.Response;
                else return false;
            }
            catch (Exception)
            {
                if (response != null) response.Close();
                return false;
            }

            return true;
        }

        /////start
        ///
        public static string MakeRequests()
        {
            HttpWebResponse response;
            string responseText = "";

            if (Request_owa_momo_vn(out response))
            {
                responseText = ReadResponse(response);

                response.Close();
            }
            return responseText;
        }

        private static bool Request_owa_momo_vn(out HttpWebResponse response)
        {
            response = null;

            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://owa.momo.vn/api");

                request.ContentType = "application/json";
                request.Headers.Set(HttpRequestHeader.Authorization, "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoiMDEyMzQ4ODgwMDgiLCJwaW4iOiIyNjA1ODciLCJpbWVpIjoiZmU5NGIwODYtYWRhOC00Yzg5LTgxMDUtZjUxNTFlMmZhZTViIiwiaWF0IjoxNTcyMzMyODA1LCJleHAiOjE1NzIzMzgyMDV9.yvqF72HYwf8NqHf2Sfy0PKHlj1SQBvQfmelUGYSxYbY");
                request.UserAgent = "PostmanRuntime/7.18.0";
                request.Accept = "*/*";
                request.Headers.Set(HttpRequestHeader.CacheControl, "no-cache");
                request.Headers.Add("Postman-Token", @"9b6fd2e8-f989-438b-83fe-e175d7d0e355");
                request.Headers.Set(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                request.KeepAlive = false;

                request.Method = "POST";
                request.ServicePoint.Expect100Continue = false;

                string body = @"{
                    ""appCode"": ""2.1.27"",
                    ""appVer"": 21271,
                    ""channel"": ""APP"",
                    ""cmdId"": ""1572320500951000000"",
                    ""deviceOS"": ""ANDROID"",
                    ""errorCode"": 0,
                    ""errorDesc"": """",
                    ""lang"": ""vi"",
                    ""momoMsg"": {
                        ""_class"": ""mservice.backend.entity.msg.TranHisMsg"",
                        ""billId"": ""121379176"",
                        ""category"": 4,
                        ""clientTime"": 1572320500950,
                        ""desc"": ""Ví MoMo"",
                        ""extras"": ""{\""vpc_CardType\"":\""SML\"",\""vpc_TicketNo\"":null}"",
                        ""fee"": 0,
                        ""giftId"": """",
                        ""moneySource"": 1,
                        ""pageNumber"": 1,
                        ""parentTranType"": 3,
                        ""partnerCode"": ""momo"",
                        ""quantity"": 1,
                        ""rowCardId"": null,
                        ""serviceId"": ""finstar"",
                        ""serviceName"": ""Doctor Đồng"",
                        ""tranType"": 7,
                        ""useVoucher"": 0,
                        ""user"": ""0834888008""
                    },
                    ""msgType"": ""NEXT_PAGE_MSG"",
                    ""result"": true,
                    ""time"": 1572320500951,
                    ""user"": ""0834888008""
                }";
                byte[] postBytes = System.Text.Encoding.UTF8.GetBytes(body);
                request.ContentLength = postBytes.Length;
                Stream stream = request.GetRequestStream();
                stream.Write(postBytes, 0, postBytes.Length);
                stream.Close();

                response = (HttpWebResponse)request.GetResponse();
            }
            catch (WebException e)
            {
                if (e.Status == WebExceptionStatus.ProtocolError) response = (HttpWebResponse)e.Response;
                else return false;
            }
            catch (Exception)
            {
                if (response != null) response.Close();
                return false;
            }

            return true;
        }

        //call login
        private static string getLoginMomo(string sUser, string sPass)
        {
            HttpWebResponse response;
            string responseText = "";

            if (Request_owa_momo_vn_login(out response, sUser, sPass))
            {
                responseText = ReadResponse(response);

                response.Close();
            }

            return responseText;
        }

        private static bool Request_owa_momo_vn_login(out HttpWebResponse response, string sUser, string sPass)
        {
            response = null;

            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://owa.momo.vn/public/login");

                request.ContentType = "application/json";
                request.UserAgent = "PostmanRuntime/7.18.0";
                request.Accept = "*/*";
                request.Headers.Set(HttpRequestHeader.CacheControl, "no-cache");
                request.Headers.Add("Postman-Token", @"f64552d3-e293-4536-b635-c4b31f988278");
                request.Headers.Set(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                request.KeepAlive = false;

                request.Method = "POST";
                request.ServicePoint.Expect100Continue = false;

                string body = @"{
                    ""appCode"": ""2.1.27"",
                    ""appVer"": 21271,
                    ""channel"": ""APP"",
                    ""cmdId"": ""1572342250418000000"",
                    ""deviceOS"": ""ANDROID"",
                    ""errorCode"": 0,
                    ""errorDesc"": """",
                    ""extra"": {
                        ""AAID"": ""1e1f968c-564a-4ef1-943f-a5fdb43951fa"",
                        ""IDFA"": """",
                        ""SIMULATOR"": ""false"",
                        ""checkSum"": ""ZbUjyZhtcQT/HIz6FoRUSnziffwQG2mLL9J5T895TOQgM1+7vpOQh8tgoKrwXckbbOgA8aosw/toOsDf3OKwIQ=="",
                        ""pHash"": ""DY+PKjgk7w2wBpineU7hpskeTBbHYifalDiDkmiikm4jtr6/KKD36I0i8/jWep91""
                    },
                    ""lang"": ""vi"",
                    ""momoMsg"": {
                        ""_class"": ""mservice.backend.entity.msg.LoginMsg"",
                        ""isSetup"": false
                    },
                    ""msgType"": ""USER_LOGIN_MSG"",
                    ""pass"": """ + sPass + @""",
                    ""result"": true,
                    ""time"": 1572342250418,
                    ""user"": """ + sUser + @"""
                }";
                byte[] postBytes = System.Text.Encoding.UTF8.GetBytes(body);
                request.ContentLength = postBytes.Length;
                Stream stream = request.GetRequestStream();
                stream.Write(postBytes, 0, postBytes.Length);
                stream.Close();

                response = (HttpWebResponse)request.GetResponse();
            }
            catch (WebException e)
            {
                if (e.Status == WebExceptionStatus.ProtocolError) response = (HttpWebResponse)e.Response;
                else return false;
            }
            catch (Exception)
            {
                if (response != null) response.Close();
                return false;
            }

            return true;
        }

    }
}
