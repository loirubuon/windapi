﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace CheckLink
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Checking api");
            while (true)
            {
                StringBuilder sb = new StringBuilder();
                DateTime sStart = DateTime.Now;
                Console.WriteLine("Start ... " + sStart.ToString("dd/MM/yyyy HH:mm:ss"));
                sb.AppendLine("Start : " + sStart.ToString("dd/MM/yyyy HH:mm:ss"));
                string s = MakeRequests();

                
                if (s.Length > 200)
                    sb.Append(s.Substring(0, 200));

                DateTime sEnd = DateTime.Now;
                sb.AppendLine("Start : " + sEnd.ToString("dd/MM/yyyy HH:mm:ss"));
                // flush every 20 seconds as you do it
                File.AppendAllText("log.txt", sStart.ToString("dd/MM/yyyy HH:mm:ss") + "////" + sb.ToString() + "////" + sEnd.ToString("dd/MM/yyyy HH:mm:ss") + "////" + (sEnd - sStart).TotalMilliseconds.ToString());
                sb.Clear();
                
                Console.WriteLine(s);
                Console.WriteLine((sEnd - sStart).TotalMilliseconds.ToString());
               
                Console.WriteLine("End ..." + sEnd.ToString("dd/MM/yyyy HH:mm:ss"));
                System.Threading.Thread.Sleep(30000);
            }
        }

        private static string MakeRequests()
        {
            HttpWebResponse response;
            string responseText = "";

            if (Request_10_2_10_14_8281(out response))
            {
                responseText = ReadResponse(response);

                response.Close();
            }

            return responseText;
        }

        private static string ReadResponse(HttpWebResponse response)
        {
            using (Stream responseStream = response.GetResponseStream())
            {
                Stream streamToRead = responseStream;
                if (response.ContentEncoding.ToLower().Contains("gzip"))
                {
                    streamToRead = new GZipStream(streamToRead, CompressionMode.Decompress);
                }
                else if (response.ContentEncoding.ToLower().Contains("deflate"))
                {
                    streamToRead = new DeflateStream(streamToRead, CompressionMode.Decompress);
                }

                using (StreamReader streamReader = new StreamReader(streamToRead, Encoding.UTF8))
                {
                    return streamReader.ReadToEnd();
                }
            }
        }

        private static bool Request_10_2_10_14_8281(out HttpWebResponse response)
        {
            response = null;

            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://10.2.10.14:8281/NAMAAPI/LinkService?wsdl");

                request.UserAgent = "PostmanRuntime/7.19.0";
                request.Accept = "*/*";
                request.Headers.Set(HttpRequestHeader.CacheControl, "no-cache");
                request.Headers.Add("Postman-Token", @"db899dfe-f4ec-4870-bb34-7eb1da04adfa");
                request.Headers.Set(HttpRequestHeader.AcceptEncoding, "gzip, deflate");

                request.KeepAlive = true;

                response = (HttpWebResponse)request.GetResponse();
            }
            catch (WebException e)
            {
                StringBuilder sb = new StringBuilder();
                File.AppendAllText("log.txt", e.ToString());
                sb.Clear();
                Console.WriteLine("1" + e.ToString());
                if (e.Status == WebExceptionStatus.ProtocolError) response = (HttpWebResponse)e.Response;
                else return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine("2" + ex.ToString());
                if (response != null) response.Close();
                return false;
            }

            return true;
        }
    }
}
